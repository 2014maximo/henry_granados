import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './app.reducer';
import * as actions from './traductor/traductor.actions';
import { ingles } from './traductor/traductor.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  texto: string;
  
  constructor( private store: Store<AppState> ) {
    
    this.store.select('texto')
      .subscribe( texto => this.texto = texto);
  }

  espanol(){
    this.store.dispatch( actions.espanol());
  }
  ingles(){
    this.store.dispatch( actions.ingles());
  }
}
