import { createAction } from '@ngrx/store';

export const espanol = createAction('[Traductor] Espanol');
export const ingles = createAction('[Traductor] Ingles');
export const frase = createAction('[Traductor] Frase');
export const reset = createAction('[Traductor] Reset');

