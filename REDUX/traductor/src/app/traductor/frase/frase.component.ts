import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/app.reducer';
import { frase } from '../traductor.actions';
import * as actions from '../../traductor/traductor.actions';

@Component({
  selector: 'app-frase',
  templateUrl: './frase.component.html',
  styleUrls: ['./frase.component.scss']
})
export class FraseComponent implements OnInit {

  texto: string;


  constructor( private store: Store<AppState>) {
    this.store.select('texto')
      .subscribe( texto => this.texto = texto);
   }

  ngOnInit(): void {

  }

  frase(){
    this.store.dispatch( actions.frase());
  }

  reset(){
    this.store.dispatch( actions.reset());
  }

}
