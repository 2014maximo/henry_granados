import { createReducer, on } from '@ngrx/store';
import { espanol, ingles, frase, reset } from './traductor.actions';



export const initialState = 'Estado inicial';
;

const _traductorReducer = createReducer(
    initialState,
    on( espanol, state =>  state = 'Hola Mundo'),
    on( ingles, state =>  state = 'Hello World'),
    on( frase, state =>  state = 'El que persevera alcanza'),
    on( reset, state =>  state = initialState),

);

export function traductorReducer(state, action) {
    return _traductorReducer(state, action);
}