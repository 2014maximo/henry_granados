import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { switchMap, map, catchError } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Actions, Effect, ofType } from '@ngrx/effects';

import * as fromCustormersAction from '../actions/customer.action';
import { CustomerService } from '../../services/customer.service';

@Injectable({
    providedIn: 'root'
})
export class CustomerEffects {

    constructor( private actions$: Actions,private customerService : CustomerService ) { }
    
    // Los effects son básicamente un servicio
    @Effect()
    loadCustomers$ : Observable<Action> = this.actions$.pipe(
        ofType(fromCustormersAction.LOAD_CUSTOMERS), // Aquí se le esta pasando la acción.
        switchMap( () => this.customerService.getCustomers()
        .pipe(
            map( response =>{
                return new fromCustormersAction.LoadCustomerSuccess(response)// Si es correcto
            },
            catchError(error => of(new fromCustormersAction.LoadCustomerFail(error) ))// Si falla devuelve error
            ),
        ))
    )
}