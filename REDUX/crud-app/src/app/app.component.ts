import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as fromStore from './store';
import { Customer } from './models/customer.model';
import { getCustomers } from './store/reducers/app.reducer';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public customers: Customer[];

  constructor(private store: Store<fromStore.AppState>) {
    store.select(fromStore.getCustomers).subscribe( rs => {
      this.customers = rs;
    });

    store.select(fromStore.getCustomerById(2)).subscribe( rs => {
      console.log(rs, 'DATOS DEL ID 2');
    });
  }

  ngOnInit() {
    this.store.dispatch(new fromStore.LoadCustomer());
  }
}
