import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Customer } from '../models/customer.model';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor( private httpClient: HttpClient) { }

  getCustomers(){
    return this.httpClient.get<Customer[]>('http://localhost:3000/usuarios');
  }
}
